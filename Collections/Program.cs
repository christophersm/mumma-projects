﻿using System;

namespace Collections {
    class Program {
        static void Main (string[] args) {
            //Arrays
            string[] summerStrut;

            summerStrut = new string[] { "Another One Bites The Dust", "Livin' On a Prayer", "Jump", "Photograph", "I Love Rock 'n' Roll", "Eye Of The Tiger", "Sweet Child of Mine", "Every Breath You Take" };

            int[] ratings = { 1, 5, 4, 3, 2, 5, 5, 8 };

            if (summerStrut.Length <= 6)
            {
                Console.WriteLine ("Ready to play!");
            }
            else if (summerStrut.Length > 6)
            {
                Console.WriteLine ("You selected too many songs for the playlist!");
            }
            else
            {
                Console.WriteLine ("Add some songs to your playlist!");
            }

            // Prints out the Song title and their rating!
            Console.WriteLine ($"{summerStrut[0]} has a rating of {ratings[0]} out of 5!");
            Console.WriteLine ($"{summerStrut[1]} has a rating of {ratings[1]} out of 5!");
            Console.WriteLine ($"{summerStrut[2]} has a rating of {ratings[2]} out of 5!");
            Console.WriteLine ($"{summerStrut[3]} has a rating of {ratings[3]} out of 5!");
            Console.WriteLine ($"{summerStrut[4]} has a rating of {ratings[4]} out of 5!");
            Console.WriteLine ($"{summerStrut[5]} has a rating of {ratings[5]} out of 5!");
            Console.WriteLine ($"{summerStrut[6]} has a rating of {ratings[6]} out of 5!");
            Console.WriteLine ($"{summerStrut[7]} has a rating of {ratings[7]} out of 5!");

            // I was experimenting with a concept covered in the course. I do not quite understand this and will revisit this in the future.
            int fiveRating = Array.IndexOf (ratings, 5);
            Console.WriteLine ($"Song number {fiveRating + 2} {summerStrut[2]} is rated 4 stars!");

            //Array built-in method - SORT
            //If integer values, it will sort lowest to highest. If they are string values, it will be sorted alphabetically

            //Sort Songs alphabetically
            Array.Sort (summerStrut);
            string firstSong = summerStrut[0];
            string lastSong = summerStrut[7];

            //Print out the first song on the new alphabetized list.
            Console.WriteLine ($"The first song in the playlist is now {firstSong}.");

            //Print out the last song on the new alphabetized list
            Console.WriteLine ($"The last song in the playlist is now {lastSong}.");

        }
    }
}