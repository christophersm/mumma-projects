﻿using System;
using System.Net.Http.Headers;

namespace ConditionalStatements
{
    class Program
    {
        static void Main(string[] args)
        {
            //If..else Statements
            int socks = 1;
            if (socks <= 3)
            {
                Console.WriteLine("Time to do laundry!");
            }
            else
            {
                Console.WriteLine("You still have enough socks!");
            }

            int people = 11;
            string weather = "bad";

            if (people <= 10 && weather == "nice")
            {
                Console.WriteLine("SaladMart");
            }
            else
            {
                Console.WriteLine("Soup N Sandwich");
            }

            //Else If Statements
            int guests = 0;

            if (guests >= 4)
            {
                Console.WriteLine("Catan");
            }
            else if (guests >= 1)
            {
                Console.WriteLine("Innovation");
            }
            else // this is optional
            {
                Console.WriteLine("Solitaire");
            }


            //Ternary Operators
            int pepperScale = 4;
            string message = (pepperScale <= 3.5) ? "No Problem" : "Houston we have big problems!";
            Console.WriteLine(message);


            //Switch Statements
            Console.WriteLine("Enter your favorite movie genre to see the top movie from the genre. Enter any of the following: Drama, Comedy, Adventure, Horror, or Science Fiction:");
            string genre = Console.ReadLine();

            switch (genre)
            {
                case "Drama":
                    Console.WriteLine("Citizen Kane");
                    break;

                case "Comedy":
                    Console.WriteLine("Duck Soup");
                    break;

                case "Adventure":
                    Console.WriteLine("King Kong");
                    break;

                case "Horror":
                    Console.WriteLine("Psycho");
                    break;

                case "Science Fiction":
                    Console.WriteLine("2001: A Space Odyssey");
                    break;

                default:
                    Console.WriteLine("Not a valid genre");
                    break;
            }
        }
    }
}
