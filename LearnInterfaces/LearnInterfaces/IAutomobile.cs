﻿using System;
namespace LearnInterfaces
{
    interface IAutomobile
    {
        double Speed { get; }
        int Wheels { get; }
        void Honk();
    }
}
