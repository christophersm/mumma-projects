﻿using System;

namespace Loops
{
    class Program
    {
        static void Main(string[] args)
        {
            //While Loop
            int emails = 200;

            while(emails > 0)
            {
                emails--;
                Console.WriteLine($"One email deleted, {emails} left!");
            }
            Console.WriteLine("INBOX ZERO ACHIEVED!");
        }
    }
}
