﻿using System;

namespace DoWhileLoops
{
    class Program
    {
        //Break
        static void Main(string[] args)
        {
            bool buttonClick = false;
            int counter = 0;
      
            do
            {
                Console.WriteLine("BLARRRRRRRRR");
                counter++;

                if (counter == 4)
                {
                    break;
                }
            } 
            while (!buttonClick);
            Console.WriteLine("Time for five minute break.");
            Continue();
        }

        //Continue 
        static void  Continue()
        {
           for (int i = 0; i <= 10; i++)
           {
              if (i == 8)
              {
                  continue;
              }
            Console.WriteLine(i);
            }
        }
    }
}
