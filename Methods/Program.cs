﻿using System;

namespace Methods
{
    class Program
    {
        //Main Method. Methods break up code into reusable, functional, blocks
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();
            AnnounceName(name);
        }


        //Methods require a name and parameters. The name is used to call the method throughout the program. Parameters are values passed into the method. Methods are PascalCase. Parameter names are camelCase and must be prefixed with their data type.
        //Methods also require a return type and return statement.
        static void AnnounceName(string yourName)
        {
            Console.WriteLine("Welcome " + yourName + " to the team!");
        }
    }
}
