﻿using System;

namespace BuiltInMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            //Built-in methods that I was tought in this lesson and will be using. 
            //Math.Abs()—will find the absolute value of a number. Example: Math.Abs(-5) returns 5.
            //Math.Sqrt()—will find the square root of a number. Example: Math.Sqrt(16) returns 4.
            //Math.Floor()—will round the given double or decimal down to the nearest whole number. Example: Math.Floor(8.65) returns 8.
            //Math.Min()—returns the smaller of two numbers. Example: Math.Min(39, 12) returns 12.

            // Starting variables 
            var numberOne = 12932;
            var numberTwo = -2828472;
            var numberThree = 25.34543;
            var numberFour = -7546332;

            // Use built-in methods and save to variable 
            var numberOneSqrt = Math.Floor(Math.Sqrt(Math.Abs(numberOne)));
            var numberTwoSqrt = Math.Floor(Math.Sqrt(Math.Abs(numberTwo)));


            // Print the lowest number
            Console.WriteLine(Math.Min(numberOneSqrt, numberTwoSqrt)); //Output will equal 113
            Console.WriteLine(Math.Sqrt(numberOne)); //Output will equal 113.718951806636
            Console.WriteLine(Math.Floor(numberThree)); //Output will equal 25
            Console.WriteLine(Math.Abs(numberFour)); //Output will be 7546332

            // Using documentation

            // Raise numberOne to the numberTwo power
            Console.WriteLine(Math.Pow(numberOne, numberTwo));

            // Round numberThree up
            Console.WriteLine(Math.Ceiling(numberThree));

            // Find the laster number between numberOne and numberthree
            Console.WriteLine(Math.Max(numberOne, numberThree));
        }
    }
}
