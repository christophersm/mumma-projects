﻿using System;

namespace Logic
{
    class Program
    {
        static void Main(string[] args)
        {
            //Boolean Data Types
            //bool answerOne = true;
            //bool answerTwo = false;


            double timeToDinner = 4;
            double distance = 95;
            double rate = 30;
            double tripDuration = distance / rate;
            bool canIMakeItToDinner = (tripDuration <= timeToDinner);

            Console.WriteLine(canIMakeItToDinner);

            //Logical Operators

            bool beach = true;
            bool hiking = false;
            bool city = true;

            //Does trip location have the friends desires of city and a beach
            bool yourNeeds = (beach && city);
            bool friendNeeds = (beach && hiking);

            //Trip decision
            bool tripDecision = (yourNeeds && friendNeeds);  //evaluates to false, all desires do not line up.

            Console.WriteLine(tripDecision);
        }
    }
}
