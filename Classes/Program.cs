﻿using System;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            AppleComputer macBookPro = new AppleComputer();
            macBookPro.ComputerModel = "MacBook Pro";
            macBookPro.ComputerProcessor = "Apple Silicon";
            macBookPro.ComputerProcessorSpeed = 2.6;
            macBookPro.ComputerProcessorCores = 6;
            macBookPro.HardDriveStorage = 512;
            macBookPro.RamMemory = 32;
            macBookPro.VideoGraphicsCard = "AMD Radeon Pro 5500M";
            macBookPro.VideoGraphicsCardMemory = 4;

            Console.WriteLine(macBookPro.ComputerModel + " " + macBookPro.ComputerProcessor + " " + macBookPro.ComputerProcessorSpeed + " " + macBookPro.ComputerProcessorCores + " " + macBookPro.HardDriveStorage + " " + macBookPro.RamMemory + " " + macBookPro.VideoGraphicsCard + " " + macBookPro.VideoGraphicsCardMemory);
        }
    }

}
