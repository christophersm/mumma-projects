using System;

namespace Classes
{
  class AppleComputer
  {
        public string ComputerModel
        {
            get; set;
        }

        public string ComputerProcessor
        {
            get; set;
        }

        public double ComputerProcessorSpeed
        {
            get; set;
        }

        public double ComputerProcessorCores
        {
            get; set;
        }

        public double HardDriveStorage
        {
            get; set;
        }

        public double RamMemory
        {
            get; set;
        }

        public string VideoGraphicsCard
        {
            get; set;
        }

        public double VideoGraphicsCardMemory
        {
            get; set;
        }

    }

}
