﻿using System;

namespace WhatIsYourFavoriteNumber
{
    class Program
    {
        static void Main(string[] args)
        {

            Start:
        //ask user for their favorite number
          Console.WriteLine("Enter your favorite number!: ");

          //Converted the int into an Int32. If any character other than numeric, it will cause an error.
          double faveNumber; //double version of favorite number
          string number; //string version of favorite number
          bool parseOkay; //holds the results
          number = Console.ReadLine(); //returns the string version of the favorite number

          parseOkay = double.TryParse(number, out faveNumber); //stores the results into the parseOkay variable. This also keeps my program from crashing.

          //Setup conditional logic using and if statement. I chose to go this route because it makes the most sense to me at this point and I understand that if one this is true, the program will do that. 
            //If it isnt, it will go back to the beginning and start over.

          if (parseOkay == true)  //This means the parse succeeded
          {
              //Output - Appends the sentence before the variable.
              Console.WriteLine("Your Favorite number is " + faveNumber);
          }
          else //this means the parse failed and no valid number was entered. It returns to ask the question again.
          {
              goto Start;
          }
        }
    }
}
