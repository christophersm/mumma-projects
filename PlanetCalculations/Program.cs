﻿using System;

namespace PlanetCalculations
{
    class Program
    {
        static void Main(string[] args)
        {
            //This code was built using arithmetic operators

            //Notes - If we add two integers, it will return and integer everytime. If we add an integer with a double, it will be a double. C# follows the order of operations.

            // My age

            int userAge = 46;

            // Length of yours on Jupiter (In Earth years)

            double jupiterYears = 11.86;

            // Age on Jupiter

            double jupiterAge = userAge / jupiterYears;

            // Time to Jupiter

            double journeyToJupiter = 6.142466;

            // New Age on Earth, if on Jupiter

            double newEarthAge = userAge + journeyToJupiter;

            // New Age on Jupiter

            double newJupiterAge = newEarthAge / jupiterYears;

            // Log all calculation to console

            Console.WriteLine(userAge);
            Console.WriteLine(jupiterYears);
            Console.WriteLine(jupiterAge);
            Console.WriteLine(journeyToJupiter);
            Console.WriteLine(newEarthAge);
            Console.WriteLine(newJupiterAge);
        }
    }
}
