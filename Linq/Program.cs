﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {
        static void Main()
        {
            string[] marvelheroes = { "Antman", "Hulk", "Ironman", "Thor", "Vision", "Wanda", "Captain America" };

            string[] marvelVillains = { "Galactus", "Red Skull", "Loki", "Thanos", "Magneto", "Doctor Doom", "Ultron" };

            // Method syntax
            var result = marvelheroes.Select(mh => $"Introducing Avenger...{mh}!");
            
            // Query syntax
            var result2 = from mh in marvelheroes
                where mh.Contains("V")
                select $"{mh} contains a 'V' in their name.";
            
            // Printing...
            Console.WriteLine("'result': ");
                foreach (var h in result)
                {
                    Console.WriteLine(h);
                }
            
            Console.WriteLine("\n'result2': ");
                foreach (var h in result2)
                {
                    Console.WriteLine(h);
                }

            // Method syntax
            var result3 = marvelVillains.Select(mv => $"Introducing their enemies...{mv}!");

            //Printing
            Console.WriteLine("'result3': ");
                foreach (var v in result3)
                {
                    Console.WriteLine(v);
                }

        }
    }
}
