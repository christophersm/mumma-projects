# 9/2020

## Classes

### What I learned

#### Overview
    - How to define a class
    - Add members, like Properties and Methods, to a class
    - Configuring access to members using Public and Private
    - Create Objects from a class

#### Notes

    - Class: represents a custom data type. In C#, the class defines the kinds of information and methods included in a custom type.
    - Fields: are a type of class member - it is common practice to name fields with lowercase (apple instead of Apple)
        - Ex
        class Apple {
            public string model;
            public int number;
        }
    - Class Member: general term for the building blocks of a class
    - Instances: there can be many instances of the same class, all with unique values.

        ex structure of a class:

        class Apple {

        }
    - The code for a class is usually put into a file of its own, named with the name of the class. In the case of the example above, Apple.cs. This keeps the code organized and easy to debug.

    - You can use the class in other parts of code. You make instances, or objects, of the Apple class with the "new" keyword.

        ex.
        Apple a = new Apple();

        a is an instance, or type, of Apple

    - Instantiation: The process of creating an instance.

    - Properties: They are another type of class member. It controls the access (getting and setting) to that field. Properties can be used to validate values before they are set to a field.
        Get and Set Methods
            - Get - a method, or getter, called when the property is accessed.
            - Set - a method, or setter, called when the property is assigned a value.
            Ex. 
            public int area;
            public int Area;
            {
                get { return area; }
                set { area = value; }
            }
        
             The "Area" property is associated with the "area" field. It is common to name a property with the title-cased version of it's field name.

             set() method - uses the keyword "value". This represents the value that is assigned to the property.
    
    - Automatic Properties: the basic getter and setter pattern short-hand.
            Ex.
            public int Area
            { get; set; }

    - Public vs. Private access modifiers
        - public: can be accessed by any class.
        - private: can only be accessed by code in the same class.
        Ex.
        public int Area
        private int Area

    - Get-Only Properties: allow to you only get the value of the property (area) and not allow the program to set the value of the property (Area). You would need to design another method of setting the field. This can be done, either through the constructor or a method on the class.
        Ex.
        public int Area;
            {
                get { return area; }
                set { area = value; }
            }
    - Methods: Way to organize chunks of code to perform a task. Also used to define how an instance of a class behaves.
        Ex.
        public int Area
        {
                get { return area; }
            }
        public int IncreaseArea(int growth)
        {
            Area = Area + growth;
            return Area;
        }

        - Call the method:
        Forest f = new Forest();
        int result = f.IncreaseArea(2);
        Console.WriteLine(result);

    - Constructors: method that runs every time an object is created and sets the property values for the object.
        Ex.
        class Forest
        {
            public int Area;

            public Forest(int area)
            {
                Area = area;
            }
        }

        - The constructor method is used whenever we instantiate an object with the "new" keyword.
        Ex. 
        Forest f = new Forest(400);

    - Parameterless Constructor: If no constructor is defined in a class, one is automatically created for us. It takes no parameters.

    - this: Used to refer to the current instance of a class, so there is no misinterpretation of what class to use.
        Ex.
        class Forest
        {
            public int Area;

            public Forest(int area)
            {
                this.Area = area;
            }
        }

        - this.Area = area means, when this constructor is used to make a new instance, user the argument "area" to set the value of the new instance's "Area" field. 
        -Another way to call this:

        Forest f = new Forest(400);

        - f.Area now equals 400.

### Take A Ways

    - Usually, if you declare class fields, they are marked as "private" and only accessed via the "Property"

### [Issues Encountered]

    - Classes make sense to me. I definitely revisited this section more times than any previous section. I have been trying to commit it to memory. I think this has stressed me out because it isn't sticking. So, I am submitting my pull request with my project.

    - I will say, I have learned to get better at debugging and fixing issues. I was able to figure out my misunderstanding of the get and set methods to get them to work properly.

## GitLab Project Link

## [Additional Notes]
