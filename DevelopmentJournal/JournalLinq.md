# 1-31-2021

## Linq

## [Issues Encountered]

## [What I learned]

- What is LINQ: Language-Intergrated Query - is the name for a set of technologies based on the integration of query capabilities directly into the C# language.

- How to import the LINQ features into C#.
- How to run LINQ queries on datasets.
- How to identify method and query symtax.
- Basic operators, such as Select, Where and From.

- IEnumerable<T> - It works with foreach loops, just like arrays and lists. You can check it's length using Count().
    - It is an interface that insures a given class is iterable. It will iterate through a list, array and a set. 
    - Can be thought of and used as a sequence of elements.
    - It will insure that the list, array or sorted set start at the first item and move to the second, third and so on to complete the items.


- Ex. of IEnumerable using list, array, and set

    ```csharp
        IEnumerable<int> list = new List<int> { 1, 2, 3 };
        IEnumerable<int> array = new[] { 1, 2, 3 };
        IEnumerable<int> set = new SortedSet<int> { 1, 2, 3 };
    ```

- Ex. of IEnumberable using foreach loop

    ```csharp
         public IEnumerator GetEnumerator()
        {
            foreach(Student student in studentList)
            yield return student; 
        }
    ```

- It is common to store the query's returned value in a variable of the type var. We let the C# compiler determine the actual type for us.
    - ex. string[] names = { "Tiana", "Dwayne", "Helena" };
    var shortNames = names.Where(n => n.Length < 4);
    (shortNames is a type of IEnumberable<string>, do not need to worry about that since we have var)

- In LINQ, we see where/Where() and select/Select() show up as both keywords and method calls. To cover both cases, they’re generally called operators.
    - Where(): is written as a method, which takes a lambda expression as an argument.
        - ex. var SuperHeroes = Heros.Where(h => h.Lenmgth < 5);
    - where: picks elements from the sequence if they satisfy the given condition.

- Method Syntax - Method syntax looks like plain old C#. We make method calls on the collection we are querying

```SQL
    var longHeroes = heroes.Where(h => h.Length > 6);
    var longLoudHeroes = longHeroes.Select(h => h.ToUpper());
```

- Query Syntax - Query syntax looks like a multi-line sentence. If you’ve used SQL, you might see some similarities

```SQL
    var longLoudHeroes = from h in heroes
    where h.Length > 6
    select h.ToUpper();
```

### Basic Query Syntax

- Basic LINQ query, in query syntax, has 3 parts:
    - 1. - from - Operator that declares a variable to iterate through the sequence. Ex. h is used in the example above to iterate through heros.
    - 2. - where - Operator picks the elements from the sequence if they satisfy the given condition. Ex. where h.length > 6
    - 3. - select - Operator determines what is returned for each element in the sequence.

- Basic Method query - each query operator is written as a regular method call. ex:

```csharp
    string[] heroes = { "D. Va", "Lucio", "Mercy", "Soldier 76", "Pharah", "Reinhardt" };
    var shortHeroes = heroes.Where(h => h.Length < 8);
```

- The where operator is written as the method Where(), which takes a lambda expression as an argument. The method returns true if h is less than 8 characters long.

    Predicate - a function that evaluates to true or false. 

- Where() calls this lambda expression for every element in heroes. If it returns true, then the element is added to the resulting collection.

    - Lambda Expression: is a way of defining an anonymous function that can be password around as a variable, or parameter, to a method call. You use the => (lambda declaration operator) to separate the lambda's parameter list from it's body. (input-parameters) => expression.

- where and Where() are the same thing. During the build process the "where" is turned into a method invocation of Where().


### Combining Select and Where

- Select() and Where() can be combined using two different ways. Either way is acceptable.

    - 1. - Use separate statements

        ```csharp
            var longHeroes = heroes.Where(h => h.Length > 6);
            var longLoudHeroes = longHeroes.Select(h => h.ToUpper());
        ```

    - 2. - Chain the expressions together

        ```csharp
            var longLoudHeroes = heroes
            .Where(h => h.Length > 6)
            .Select(h => h.ToUpper());
        ```
    
    - When to use each syntax:

        - 1. - For single operator queries, use the method syntax.
        - 2. - For everything else, use the query syntax.

### LINQ with other collections

- Linq can be used with lists and other data types also. The Syntax is the same.

## [Additional Notes]