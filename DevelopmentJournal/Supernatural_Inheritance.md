# 1-30-2021

## Inheritance

## [Issues Encountered]

- This project walk through helped with understanding a very complicated subject. The understanding what members can do what and how. 

## [What I learned]

- Inheritance is a way to avoid duplication across multiple classes.
- Inheritance, one class inherits the members of another class.
- Subclass, or derived class, is the class that inherits
- Superclass, or base class, is the class the subclass inherits from.
- Superclass members can be accessed using base.
- Restricted access to a superclass and it's subclasses can be set by using protected.
- A superclass member can be overridden by using virtual or override keywords in the methods.
- You can make a member in a superclass without defining it's implementation using abstract.

## [Additional Notes]
