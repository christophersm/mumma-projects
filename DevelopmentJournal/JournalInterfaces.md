# Interfaces

## Prerequisites

    - Classes
    - Inheritance
    - Properties
    - Methods

## Definitions

    - Interfaces - sets of actions and values that describe how a class can be used. This lets the computer check that we are using each type legally, thus preventing a whole group of type errors.
        * Every interface should have a name starting with "I". This is a useful reminder to other developers, and future selves, that this is an interface and not a class.
        * An interface is a set of actions and values, but it doesn't specify how they work.
        * A class implements an interface if it defines the properties, methods and other members needed.


## [What I learned]

    - How to build an interface

    ex.
    interface IAutomobile
    {
    }
    
    - We must announce that a class implements an interface using the colon. This empty class promises to implement the IAutomobile interface. It must have the properties and methods the product owner asked for. In this example, it is (Speed, LicensePlate, Wheels, and Honk()). If these are not met, you receive an error: error CS0535: Sedan does not implement interface member 'IAutomobile.LicencePlate'.
    
    ex. 
    class Sedan : IAutomobile
    {
    }

    - To fix this error, in the above example, you need to define the members in the interface. The , included, members must be public, so the product owner can access them.

    class Sedan: IAutomobile
    {
        public string LicensePlate
        { get; }

        public double Speed
        { get; }

        public int Wheels
        { get; }

        public void Honk()
        {
            Console.WriteLine("HONK!");
        }
    }

    - What Interfaces cannot do:

        * Interfaces cannot specify two types of members that are commonly found in classes:
            1. - Constructors
            2. - Fields

## [Project I created and gitlab link]

## [Additional Notes]

