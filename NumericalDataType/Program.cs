﻿using System;

namespace NumericalDataType
{
    class Program
    {
        static void Main(string[] args)
        {
            // Number of Pizza Shops
            int pizzaShops = 4332;

            //Number of employees
            int totalEmployees = 86928;

            //Revenue
            decimal revenue = 390819.28m;

            // Log the values to the console
            Console.WriteLine(pizzaShops);
            Console.WriteLine(totalEmployees);
            Console.WriteLine(revenue);
        }
    }
}
