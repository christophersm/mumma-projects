﻿using System;

namespace WorkingWithText
{
    class Program
    {
        static void Main(string[] args)
        {
            string camera = "Nikon";
            string myCameraGear = $"\"I was looking into upgrading my {camera}\" soon! \nI was investigating many different options in the full-frame model line. \nI was leaning towards the {camera} Z6, but was able to find a {camera} Z7 for a steal of a price!";

            int storyLength = myCameraGear.Length;
            string toFind = "a steal of a price!";

            string findLowerCase = toFind.ToLower();
            int CameraGear = myCameraGear.IndexOf(toFind);

            Console.WriteLine(myCameraGear.Substring(CameraGear));
            Console.WriteLine($"This scene is {storyLength} long.\n");
            Console.WriteLine($"The term we're looking for is {toFind} and is located at index {CameraGear}.");

            //Building Strings
            // First string variable
            string firstSentence = "It is a truth universally acknowledged, that a single man in possession of a good fortune, must be in want of a wife.";
            // Second string variable
            string lastSentence = "With the Gardiners, they were always on the most intimate terms. Darcy, as well as Elizabeth, really loved them, and they were both sensible of the warmest gratitude towards persons who, by bringing her into Derbyshire, had been the means of uniting them.";

            // Print variable and newline
            Console.WriteLine(firstSentence);
            Console.WriteLine("\n");
            Console.WriteLine(lastSentence);

            //String Concatenation

            // Concatenate the string and the variables
            string beginning = "My favorite dessert";
            string middle = "New York Style Cheesecake,";
            string end = "with caramel sauce!";

            string story = beginning + " is " + middle + " drizzled " + end;

            // Print the story to the console 
            Console.WriteLine(story);

            //String Interpolation
            // Declare the variables
            string thebeginning = "Once upon a time,";
            string themiddle = "A kid, named Chris, climbed to the top of the tree. ";
            string theend = " It was like he could see the whole world!";

            // Interpolate the string and the variables
            string thestory = $"{thebeginning} there was a very tall tree. {themiddle} When he got to the top, {theend}";


            // Print the story to the console
            Console.WriteLine(thestory);

            //String Length
            // Create password
            string password = "a92301j2add";

            // Get password length
            int passwordLength = password.Length;
            Console.WriteLine(password.Length); // returns the password length.

            // Check if password uses symbol
            int passwordCheck = password.IndexOf("!");

            // Print results
            Console.WriteLine($"The user password is {password}. Its length is {passwordLength} and it receives a {passwordCheck} check.");

            // Get Parts of Strings
            //User Name
            string name = "Christopher Scott Mumma";
            int fullName = name.IndexOf(' ');

            //Get first letter
            int charPosition = name.IndexOf("C");
            char firstLetter = name[charPosition];

            // Get last name
            int namePosition = name.IndexOf("Mumma");
            string lastName = name.Substring(fullName + 7);

            // Print results
            Console.WriteLine($"The first letter of the client's first name is {firstLetter}. The client's last name is {lastName}");

            //Manipulating String
            // Script line
            string script = "Close on a portrait of the HANDSOME PRINCE -- as the BEAST'S giant paw slashes it.";

            // Get camera directions
            int charPositions = script.IndexOf("Close");
            int length = "Close on".Length;
            string cameraDirections = script.Substring(charPositions, length);

            // Get scene description
            charPositions = script.IndexOf("a portrait");
            string sceneDescription = script.Substring(charPositions);

            // Make camera directions uppercase
            cameraDirections= cameraDirections.ToUpper();

            // Make scene description lowercase
            sceneDescription = sceneDescription.ToLower();

            // Print results

            Console.WriteLine($"{cameraDirections} are {sceneDescription}");
        }
    }
}
