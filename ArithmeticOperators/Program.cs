﻿using System;

namespace ArithmeticOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            // Calculating Net Income
            double revenue = 853023.45;
            double expenses = 438374.11;
            double netIncome = revenue - expenses;

            Console.WriteLine(netIncome);

            //Calculating the break-even point
            double fixedCosts = 912849.30;
            double salesPrice = 29.99;
            double variableCostPerUnit = 17.65;

            double breakEvenVolume = fixedCosts / (salesPrice - variableCostPerUnit);
            Console.WriteLine(breakEvenVolume);
        }
    }
}
