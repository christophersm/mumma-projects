# Learning-Notes

- Research Best Practices for Repository Structure
- Repository structure can be a very opinionated subject, however having connections from the start makes any new repositories easier to build up since you have a convention (or a reference doc) in play. Research various best practices how to arrange a .net core or C# program.

## Questions to be answered

- What kind of folders?
- What kind of md files?
    1. - .csproj
    2. - .sln
    3. - program.cs

- Repository size? should it be big, super small, or just right?
- Use this convention to organize your projects, source files, markdown based upon that research. These convention pays off in the future such that.
- Code and md are easier to find and consistent build scripts etc can be reused since they all look the same.
