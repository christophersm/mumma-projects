﻿using System;

namespace Modulo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Modulo is the same as a percentage symbol. However, it calculates how many times a number fits into the larger number.
            // Number of people who attended WWDC in 2018
            //int people = 1350;

            // Available sessions for 2020
            //int sessionsAvailable = 100;

            // Number of people per session
            //Console.WriteLine((people % sessionsAvailable) + " Attendees remaining who are not able to attend a session!");

            //When this Modulo runs it will calculate how many times session available will go into people. Then it will output the remainder as attendees who are unable to attend the event.

            //Prints every twealth number from 0 up to and less than 300.
            for (int i = 0; i < 300; i++)
            {
                if ((i % 12) == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
