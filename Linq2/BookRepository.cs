using System.Collections.Generic;

namespace Linq2
{
    public class BookRepository
    {
        public IEnumerable<Book> GetBooks()
        {
            return new List<Book>
            {
                new Book() {Title = "Thr3e", Author = "Ted Dekker", Genre = "Thrillers", Price = 8.00f },
                new Book() {Title = "Black", Author = "Ted Dekker", Genre = "Fantast&Speculative", Price = 11.69f },
                new Book() {Title = "Hacker: The Outlaw Chronicles", Author = "Ted Dekker", Genre = "Fantast&Speculative", Price = 14.99f },
                new Book() {Title = "Outlaw", Author = "Ted Dekker", Genre = "Fantast&Speculative", Price = 8.94f },
                new Book() {Title = "Red", Author = "Ted Dekker", Genre = "Fantast&Speculative", Price = 7.49f }
            };
        }
    }
}