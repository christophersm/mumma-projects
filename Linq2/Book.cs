namespace Linq2
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public float Price { get; set; }
    }
}