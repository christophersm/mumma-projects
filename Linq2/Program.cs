﻿using System;
using System.Linq;

namespace Linq2
{
    class Program
    {
        static void Main(string[] args)
        {
            var books = new BookRepository().GetBooks();

            // LINQ Query Operator was used
            var cheapestBooks = from b in books
                                where b.Price < 10
                                orderby b.Title
                                select b;

            // Linq Extension Method was used
            // Display books that are less than $10
            var cheapBooks = books
                                .Where(b => b.Price < 12)
                                .OrderBy(book => book.Price);

            // Display books that have the Genre of Thriller
            var thrillerBooks = books.Where(t => t.Genre == "Thrillers");

            //Returns a single book
            var SingleBook = books.Single(b => b.Title == "Red");

            // If no objects match the condition, we want to avoid an exception by returning null, instead of an expected value.
            var SingleBook2 = books.SingleOrDefault(b => b.Title == "Scared");

            // Skip objects in the list to return objects after them. Will skip the first two books and return the information for the last 3 books
            var BookList = books.Skip(2).Take(3);

            // Count the total number of books
            var count = books.Count();


                foreach (var book in cheapBooks)
                {
                    Console.WriteLine("Cheap Books: " + book.Title + " " + book.Price);
                }
        

                foreach (var book in cheapestBooks)
                {
                    Console.WriteLine("Cheapest Books: " + book.Title + " " + book.Price);
                }

                foreach (var book in thrillerBooks)
                {
                    Console.WriteLine(book.Title + " " + book.Genre);
                }

                Console.WriteLine("The book with red in the title: " + SingleBook.Title);

                Console.WriteLine(SingleBook2 == null);

                foreach (var b in BookList)
                {
                    Console.WriteLine(b.Title);
                }

                Console.WriteLine("Total number of books in the collections: " + count);
        }
    }
}
